
require('Prototype')

if (!window['swan']) {
    window.CC_BAIDUGAME = 0
}
if (!window['tt']) {
    window.CC_TOUTIAOGAME = 0
}

cc.Class({
    extends:require('Component'),

    properties: {
        uiMgr : require('UIManager'),
        ejMgr : require('EjectManager'),
        gameLoop :require('GameLoop'),
        resJson:cc.JsonAsset,
        baseJson : cc.JsonAsset
    },

    onLoad () {
        // cc.sys.localStorage.clear();
        if(window.global == null){
            window.global = {}
        }
        global.Config = require('Config')
        global.Common = require('Common');
        global.ChatMsgQueue = [];
        global.Loader = require('Loader')
        global.UIMgr = this.uiMgr;
        global.EJMgr = this.ejMgr;
        global.Event = require('EventEmiter');
        global.GameLoop = this.gameLoop;
        global.Data = {};
        global.Textures = {};
        global.WechatShare = require('WechatShare')
        global.Socket = require('SocketClient')
        global.Enum = require('Enum')
        global.DataCfg = this.baseJson.json
        window.SoundMgr = require('SoundMgr')
        var Platform = this.initPlatform();
        if (!global.Platform) {
            global.Platform = Platform;
            global.Platform.init();
        }
        
        global.Loader.init();

        // global.upgrade={
        //     "0":[1,2,3,4,5],
        //     "1":[20,30,40,50,60],
        //     "2":[1,2,3],
        //     "3":[50,100,150,200,250],
        //     "4":[5000,10000,20000,40000,80000]
        // };
    },

    initPlatform() {
        var platform = require('wy-WebTest');
        return new platform();
    },

    start () {
        
        CC_WECHATGAME && wx.showLoading({title:'加载中',mask:true})
        var self = this;
        // cc.loader.load(this.resJson,function(err,res){
            global.Resources = this.resJson.json;

            global.Loader.loadResources('Eject',function(){
                global.Loader.loadResources('Main',function(){
                    CC_WECHATGAME && wx.hideLoading()
                    global.UIMgr.pushUI('Main/Login');
                })
            })
            
        // })

        // cc.game.on(`
        // cc.game.setFrameRate(40)
    },

    pushJSON(){
        // global.Data={
        //     coin : 0,//金币
        //     mainLevel : 0,//主武器等级
        //     secondLevel : 0,//副武器等级
        //     empLevel : 0,//初始能量
        //     bombLevel : 0,//必杀技能等级
        //     plane:[1,0,0,0],//飞机是否解锁
        //     level:[1,0,0,0,0,0],//关卡是否解锁
        //     plane.gateFirst:[0,0,0,0,0,0]//关卡是否第一次通过
        // }
        // cc.sys.localStorage.setItem('globalData',JSON.stringify(global.Data))
    },

    

    onBtnStart(){
        
    },

    update (dt) {
        //cc.log('darwcall:',cc.renderer.drawCalls)
    },
});
