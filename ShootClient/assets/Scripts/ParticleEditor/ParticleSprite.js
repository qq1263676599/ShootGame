var ParticlePool = require('ParticlePool')
var FrameManager = require('FrameManager')
cc.Class({
    extends: require('FrameComponent'),

    properties: {
    },
    onLoad(){
    },
    setCallBack(cb){
        if(!cb){
        }
        this.cb = cb
    },
    initData(delta){
        // this.node._particle = particleData
        this.runTime = delta;
    },

    recovery(){
        // cc.log('recovery',this.node.name)
        this.cb&&this.cb(this);
        this.cb = null;
        ParticlePool.put(this.node._particle.poolName,this.node)
        
    },
    updateStep(dt){
        if(!this.enabled){
            return;
        }
        this.node._particle.life -= dt;
        this.node._particle.accelerationTime -= dt; 
        this.runTime += dt;
        if(this.node._particle.life < 0){
            this.recovery();
        } else {
            // var ax = Math.max(this.node._particle.accelerationTime,0) * this.node._particle.acceleration.x
            // var ay = Math.max(this.node._particle.accelerationTime,0) * this.node._particle.acceleration.y
            // var gx = this.node._particle.gravity.x * this.runTime * this.runTime / 2
            // var gy = this.node._particle.gravity.y * this.runTime * this.runTime / 2
            var x = (this.node._particle.speed.x) * dt;
            var y = (this.node._particle.speed.y) * dt;
            this.node.x += x
            this.node.y += y

            // this.node.opacity = Common.clamp(0,255,this.node.opacity+this.node._particle.opacity*dt);
            // this.node.setScale(this.node.scaleX+this.node._particle.scale.x*dt,this.node.scaleY+this.node._particle.scale.y*dt)
        }
    },

    update(dt){
        if(cc.isPauseGame || !this.autoUpdate){
            return;
        }
        if(!this.node._particle){
            return
        }
        this.updateStep(dt);
        
    },
    gameUpdate(dt){
        if(!this.node._particle){
            return
        }
        this.updateStep(dt);
    }
});
