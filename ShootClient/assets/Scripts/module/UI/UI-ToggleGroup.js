
cc.Class({
    extends: cc.Component,

    properties: {
        toggles : {
            default : [],
            type : require('UI-Toggle')
        },
        clickEvents: {
            "default": [],
            type: cc.Component.EventHandler,
            tooltip: "i18n:COMPONENT.button.click_events"
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        for(var i=0; i<this.toggles.length; i++){
            var tog = this.toggles[i];
            tog._toggleTag = i;
            this.registerToggleTouch(tog,i);
        }
        this.setIndex(0)
    },
    setIndex(index){
        this.currentIndex = index;
        for(var i=0; i<this.toggles.length; i++){
            var tog = this.toggles[i];
            tog.setToggle(index == tog._toggleTag ? 1: 0)
        }
    },
    getIndex(){
        return this.currentIndex;
    },

    addToggle(toggle,tag){
        if(!toggle){
            return;
        }
        
        this.toggles.push(toggle);
        toggle._toggleTag = tag&&(this.toggles.length)
        this.registerToggleTouch(toggle,toggle._toggleTag);
    },

    registerToggleTouch(toggle,tag){
        var handler = new cc.Component.EventHandler();
        handler.target = this.node;
        handler.component = 'UI-ToggleGroup';
        handler.handler = 'onTouchToggle';
        handler.customEventData = tag;
        toggle.clickEvents.push(handler)
    },

    onTouchToggle(e,d){
        // cc.log('d:',d);
        this.setIndex(d);
        cc.Component.EventHandler.emitEvents(this.clickEvents, d);
    },

    start () {

    },

    // update (dt) {},
});
