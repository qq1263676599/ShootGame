// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html
var ParticlePool = require('ParticlePool')
cc.Class({
    extends: cc.Component,

    properties: {

    },
    start(){
        this.launchCount = 0;
        global.Event.on('particle',this.onCreateParticle.bind(this));
    },
    onCreateParticle(str){
        // var str = this.data.spriteFrame+','+x+','+y+','+r+','+life+','+speedx+','+speedy+','+a;
        var d = str.split(',');
        var data = [d[0]]
        for(var i=1; i<d.length-1; i++){
            data[i] = parseFloat(d[i])
        }
        var sf = global.Textures[d[0]];

        this.launchCount++
        let node = ParticlePool.get(data[0],this.node,'particle'+this.launchCount,sf,this.node._skillBase)
        node.x = data[1];
        node.y = data[2];
        node.rotation = data[3];

        node._particle = {
            poolName : data[0],
            speed : cc.v2(data[5],data[6]),
            life : data[4],
            angle : data[7],
        }
        node.sprite.initData(data[8]);
        var skillbase = global.DataCfg.skillCfg[d[10]];
        this.addBulletConfig(node,skillbase,d[9])
    },
    addBulletConfig(node,skillBase,aim){
        var ps = node.getComponent('ParticleSprite')
        ps.setAutoUpdate(false);
        let bullet = node.addComponent('Bullet')
        bullet.setCamp(skillBase.camp)
        bullet.setOptions(skillBase);
        bullet.activeColldier = true

        ps.setCallBack(function(){
            bullet.destroy();
        })
        if(skillBase.aim){
            var players = global.GameLoop.GameMgr.getPlayers()
            if(players.length <= aim){
                cc.warn('怎么回事')
                return;
            }
            var idx = aim
            var speed = skillBase.speed;
            var p1 = node.getPosition();
            var p2 = players[idx].node.getPosition();

            var len = p2.sub(p1).mag()
            var a = Math.acos((p2.x - p1.x) / len)
            var r = a;
            if (p2.y < p1.y) {
                r = 2 * Math.PI - r;
            }
            // // r = Math.PI /3;
            // cc.log('角度',r*180/Math.PI);
            var dx = Math.cos(r) * speed
            var dy = Math.sin(r) * speed
            node._particle.speed = cc.v2(dx, dy)
            node.rotation = 270 - r * 180 / Math.PI
        }
    },

    frameUpdate(data){
        for(var key in data){
            var d = data[key];
            // for(var a in d){
                this.onCreateParticle(d);
            // }
        }
    }
    
});
