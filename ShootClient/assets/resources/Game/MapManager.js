// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {

    },

    onLoad(){
        this.mapIndex = -1;
        this.lastTiledY = -1
        this._colliders = [];
        this._monsters = {};
        this._objects = {};
    },

    resetScene(map){
        // cc.log('resetScene:',data.data.data);
        for(var t in map){
            var array = t.split('-');
            this.destroyMonster({i:array[0],x:array[1],y:array[2]})
        }
    },

    updateMapCollider(){
        var y = global.GameLoop.GameMgr.camera.node.y;
        var ty = Math.floor(y / (48 * 60));
        if(ty == this.lastTiledY){
            return
        }
        this.lastTiledY = ty;

        var children = this.node.children
        for(var i=0; i<children.length; i++){
            var tmx = children[i].getComponent(cc.TiledMap);
            var boxLayer = tmx.getLayer('box');
            var wallLayer = tmx.getLayer('wall')
            var behaviorLayer = tmx.getObjectGroup('behavior')
            var objectLayer = tmx.getObjectGroup('object')

            var behaviors = {};
            for(var okey in behaviorLayer._objects){
                var obj = behaviorLayer._objects[okey];
                var ox = Math.floor(obj.x / tmx._tileSize.width);
                var oy = tmx._mapSize.height - Math.floor(obj.y / tmx._tileSize.height);
                behaviors[ox+'-'+oy] = obj;
            }
            
            
            for(var k=0; k<tmx._mapSize.width; k++){
                for(var l=0; l<tmx._mapSize.height; l++){
                    var obj1 = boxLayer.getTileGIDAt(k,l);
                    if(obj1){
                        var gdNode = boxLayer.getTiledTileAt(k,l,true)
                        gdNode.node.anchorX = 0;
                        gdNode.node.anchorY = 0.;
                        var bc = gdNode.node.addComponent('Monster');
                        this.setBoxPoint(tmx._mapSize,gdNode.node)
                        bc.setCamp(global.Enum.CAMP.neutral)
                        bc.setTiledInfo(tmx,i,k,l)
                        bc.blood = 10;
                        bc.activeColldier = true;
                        bc.rect = new cc.Rect(0,0,60,60)

                        var behavior = behaviors[k+'-'+l];
                        if(behavior){
                            bc.setBehavior(behavior)
                            bc.launcher.camp = global.Enum.CAMP.enemy;
                            // bc.active = true;
                        }

                        this._monsters[i+'-'+k+'-'+l] = bc
                    }
                    var obj2 = wallLayer.getTileGIDAt(k,l);
                    if(obj2){
                        var gdNode = wallLayer.getTiledTileAt(k,l,true)
                        var bc = gdNode.node.addComponent('Monster');
                        this.setBoxPoint(tmx._mapSize,gdNode.node)
                        bc.setCamp(global.Enum.CAMP.wall)
                        bc.setTiledInfo(tmx,i,k,l)
                        bc.tiled = gdNode
                        bc.blood = 10;
                        bc.activeColldier = true;
                        bc.rect = new cc.Rect(0,0,60,60)
                        // this._monsters.push(bc);
                    }
                }
            }

            for(var okey in objectLayer._objects){
                var obj = objectLayer._objects[okey];
                var ox = Math.floor(obj.x / tmx._tileSize.width);
                var oy = tmx._mapSize.height - Math.floor(obj.y / tmx._tileSize.height);
                // obj.monster
                var node = global.Loader.getInstantiate('Game/monster/'+obj.monster);
                node.x = obj.x - objectLayer.node.width/2
                node.y = obj.y - objectLayer.node.height/2
                objectLayer.node.addChild(node);
                this._objects[obj.id] = node.getComponent('Monster')
                // objects[ox+'-'+oy] = obj;
            }

        }
    },

    updateMapCollider1(){
        var cameray = global.GameLoop.GameMgr.camera.node.y+cc.winSize.height;//视野最大高度
        var height = this.node.height;

        for(var key in this._monsters){
            var bc = this._monsters[key];
            
            var p = bc.node.convertToWorldSpace(cc.Vec2.ZERO);
            var dy =(p.y < cameray ? 1:0)
            bc.active = dy
            if(p.y < cameray-cc.winSize.height-60){
                bc.activeColldier = false;
                delete this._monsters[key]
            }
        }
        for(var key in this._objects){
            var bc = this._objects[key];
            
            var p = bc.node.convertToWorldSpace(cc.Vec2.ZERO);
            var dy =(p.y < cameray ? 1:0)
            bc.active = dy
        }
    },  

    setBoxPoint(size,node) {
        var poAt = { x: node.x, y: node.y }
        poAt.x -= (size.width) * 30;
        poAt.y -= (size.height) * 30;
        node.x = poAt.x;
        node.y = poAt.y;

    },

    preload(gate,cb){
        // cc.loader.loadRes('map/g'+gate+'_1',cc.TMXAsset,function(){

        // })
    },

    sendDestroyMonster(monster){
        var tmxInfo = monster.tmxInfo
        global.GameLoop.GameMgr.send('dbox',{i:tmxInfo.i,x:tmxInfo.x,y:tmxInfo.y})
    },

    destroyMonster(tmxInfo){
        // cc.log('destroyMonster:',tmxInfo)
        var tmx = this.node.children[tmxInfo.i].getComponent(cc.TiledMap);
        var key = tmxInfo.i+'-'+tmxInfo.x+'-'+tmxInfo.y
        var ms = this._monsters[key];
        if(ms){
            ms.node.destroy();
            delete this._monsters[key];
        }

        // var tmx = monster.tmxInfo.tmx
        var x = tmxInfo.x
        var y = tmxInfo.y

        var boxLayer = tmx.getLayer('box');
        var boxMaskLayer = tmx.getLayer('box1');
        if(boxLayer.getTileGIDAt(x,y)){
            boxLayer.setTileGIDAt(0,x,y);
        }
        if(boxMaskLayer.getTileGIDAt(x,y-1,true)){
            boxMaskLayer.setTileGIDAt(0,x,y-1);
        }
    },

    updateBehavior(dt){
        for(var key in this._monsters){
            this._monsters[key].gameUpdate(dt);
        }
    },

    gameUpdate(dt){
        this.updateMapCollider();
        this.updateMapCollider1();
        this.updateBehavior(dt);
    }

});
