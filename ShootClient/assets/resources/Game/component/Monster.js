

cc.Class({
    extends: require('BoxCollider'),

    properties: {
        active : {
            set(v){
                if(!this._active && v){
                    this._active = true;
                    this.isRunging = true;
                }
            },
            get(){
                return this._active
            }
        },
        _active : false
    },
    onLoad(){
        // this.c = 
        this.runTime = 0;
        this.currentLogic = null
        this.logicIndex = 0;
        this.hitColor = new cc.Color(255,120,120);
        this.n_hitTime = -1;
        this.tmxInfo = null
    },
    setBehavior(b){
        cc.log('setBehavior:',b);
        this.behaviors = global.DataCfg.mapbehavior[b.behavior]
        if(!this.launcher){
            this.launcher = new cc.Node('launcher').addComponent('SkillLauncher')
            this.node.addChild(this.launcher.node);
        }
        this.launcher.camp = this.camp;
        // this.isRunging = true;
    },
    setTiledInfo(tmx,i,x,y){
        this.tmxInfo = {tmx:tmx,i:i, x:x,y:y}
    },
    setObjectInfo(tmx,i,x,y){
        this.tmxInfo = {tmx:tmx,i:i, x:x,y:y}
    },

    runLogic(){
        var logic = this.currentLogic;
        if(logic.skill){
            var r = 0;
            while(logic.skill[r]){
                this.launcher.addLauncher(logic.skill[r])
                r++;
            }
        }
    },

    destroy1(){
        if(this.tmxInfo){
            this.isIgnore = true;
            var key = this.tmxInfo.i+'-'+this.tmxInfo.x+'-'+this.tmxInfo.y
            global.GameLoop.GameMgr.mapMgr.sendDestroyMonster(this);
            var data = {};
            data[key] = 0
            global.GameLoop.GameMgr.submitData({
                map:data
            })
        }
    },

    onAttack(bullet){
        if(this.camp == global.Enum.CAMP.wall){
            return;
        }
        var attack = bullet.getAttack()
        this.blood -= attack;
        if(this.blood <= 0){
            this.destroy1();
        } else {
            if(this.camp != global.Enum.CAMP.wall){
                if(this.n_hitTime == -1){
                    this.node.color = this.hitColor
                }
                this.n_hitTime = 0.1;
            }
        }
    },

    onColliderEnter(no){
    },

    gameUpdate(dt){
        if(this.n_hitTime >= 0){
            this.n_hitTime -= dt;
            if(this.n_hitTime< 0){
                this.n_hitTime = -1;
                this.node.color = cc.Color.WHITE;
            }
        }

        if(!this.isRunging || !this.behaviors){
            return;
        }
        
        this.runTime += dt;
        if(!this.currentLogic){
            this.currentLogic = global.Common.deepCopy(this.behaviors[this.logicIndex])
            this.runLogic();
        }
        this.currentLogic.time -= dt;
        if(this.currentLogic.time < 0){
            if(typeof(this.currentLogic.to) != 'undefined'){
                this.logicIndex = this.currentLogic.to
            } else {
                this.logicIndex++;
            }
            this.launcher && this.launcher.reset();

            if(this.behaviors[this.logicIndex]){
                this.currentLogic = global.Common.deepCopy(this.behaviors[this.logicIndex])
                this.runLogic();
            } else {
                this.isRunging = false;
            }
        }
    }
});
