var LOCAL_USER = ['0','1']

cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    onClick(e,d){
        global.Socket.setOptions({id:LOCAL_USER[d],token:null})
        // global.Data._id : LOCAL_USER[d]
        global.Socket.on('loginSuccess',function(res){
            cc.log('loginSuccess:',res);
            Object.assign(global.Data,res.user);
            global.Socket.send('join',0)
            
        })
        global.Socket.on('join',function(res){
            cc.log('加入房间成功',res);
            global.UIMgr.popUI();
            global.GameLoop.enterGameScene()
        })
        global.Socket.connect(global.Config.scoketHost,global.Config.scoketPort);
    }

    // update (dt) {},
});
